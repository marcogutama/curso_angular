import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'curso';
  txtNombre:string='';
  txtApellido:string='';
  txtEdad:number=0;
  txtUser:string='';
  txtContrasena:string='';
  txtUs:string='';
  txtPass:string='';
  acceso:boolean=false; 

  registrar(){
    if(this.txtNombre!='' &&this.txtApellido!='' && this.txtEdad>0 && this.txtUs!='' && this.txtPass){
        alert("registrado");
    }else{
      alert("Completa todos los datos");
    }
  }

  entrar(){
    if(this.txtUser!='' && this.txtContrasena!=''){
      if(this.txtUs==this.txtUser && this.txtPass==this.txtContrasena){
        this.acceso=true;
      }else{
        alert("Credenciales son incorrectas");
      }
   }else{
    alert("Completa los credenciales");
   }
  }

  salir(){
    this.acceso=false;
  }
}
